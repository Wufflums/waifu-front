<html>
<head>
	<meta charset="UTF-8">
	
	<link rel="stylesheet" type="text/css" href="css/main.css">
	
	<title>Front Page</title>
</head>
<body>
	<div class="timeline">
		<div class="timeline-base"></div>
		<div class="timeline-arrow_up"></div>
		<div class="timeline-arrow_down"></div>
		<div class="timeline-node node_01">
			<div class="timeline-node_popup hidden">
				<span>Popup #1</span>
			</div>
		</div>
		<div class="timeline-node node_02">
			<div class="timeline-node_popup hidden">
				<span>Popup #1</span>
			</div>
		</div>
	</div>
	<div class="floaty_header">
		<div class="header-button">
			<a href="">
				<span>Profile</span>
			</a>
		</div>
		<div class="header-button">
			<a href="">
				<span>My Timelines</span>
			</a>
		</div>
		<div class="header-button">
			<a href="">
				<span>My Submissions</span>
			</a>
		</div>
		<div class="header-button">
			<a href="">
				<span>Forums</span>
			</a>
		</div>
		<div class="header-button">
			<a href="">
				<span>Wiki</span>
			</a>
		</div>
		<div class="header-button">
			<a href="">
				<span>Dev Blog</span>
			</a>
		</div>
		<div class="header-button">
			<a href="">
				<span>About</span>
			</a>
		</div>
	</div>
	<div class="main_image">
		<img src="img/main_image.jpg">
	</div>
</body>
</html>